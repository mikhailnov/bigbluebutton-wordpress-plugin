<?php
// Snippet for "Woody snippets" WordPress plugin
// Example: [wbcr_php_snippet id="XXX" room_id="YYYYYYYYYYYYY" lesson_type-"8-13"]
if (($room_id) && (!empty($room_id)) && ($lesson_type) && (!empty($lesson_type))) {
	$params_ok = "1";
} else {
	$params_ok = "";
}
if ($lesson_type == "8-13") {
	$positions="Вводный урок для детей 8-13 лет";
} elseif ($lesson_type == "5-7") {
	$positions="Вводный урок для детей 5-7 лет";
} else {
	$params_ok = "";
}
if (!empty($params_ok)) {
	$shortcode_main = "
		[restrict]
			<h3>Вход для залогигенных сразу в комнату</h3><br>
			[bigbluebutton token=".$room_id."]<hr>
		[/restrict]";
	echo do_shortcode($shortcode_main);
	echo '
	<form name="form2" method="get" action="https://wl.dumalogiya.ru/cgi-bin/wl.cgi">
		<input type="hidden" name="action" value="login_to_webinar" />
		<input type="hidden" name="meetingID" value="'.$room_id.'" />
		<p>Имя участника занятия (вебинара):<br>
		<input type="text" id="name" name="display_name" size="20" required>
		</p>
		<p>Ваш e-mail, с которого регистрировались на занятие:<br>
		<input type="email" id="name" name="email" size="20" required>
		</p>
		<p><input type="hidden" name="positions" value="'.$positions.'" />
		</p>
		<p>
		<input type="submit" name="SubmitForm" value="Войти на вебинар">
		</p>
	</form>
	<p>После входа, пожалуйста, включите микрофон и веб-камеру. Не открывайте несколько копий вебинарной площадки одновременно, иначе будет эхо.</p>
	<p><strong>Впервые на нашем вебинаре?</strong> Испытываете затруднения? Ознакомьтесь с <a href="https://wl.dumalogiya.ru/manual/" target="_blank">инструкцией по входу на вебинарную площадку</a>.</p>
	<p>В случае возникновения проблем напишите на <a href="mailto:info@dumalogiya.ru">info@dumalogiya.ru</a>, приложив скриншоты или фотографии, иллюстрирующие проблему, а мы постараемся помочь.</p>';
	$shortcode_rec = "
	[restrict]
		<hr>
		<h2>Записи занятий в этой комнате</h2>
		[bigbluebutton_recordings token=".$room_id."]
	[/restrict]";
	echo do_shortcode($shortcode_rec);
} else {
	echo "<p>Не переданы или имеют неверные значения параметры room_id / lesson_type в шорткоде!</p>";
}
?>
